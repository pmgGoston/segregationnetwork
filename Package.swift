// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SegregationNetwork",
    products: [.library(name: "SegregationNetwork",
                        targets: ["SegregationNetwork"]),
    ],
    targets: [.target(name: "SegregationNetwork",
                      path: "Sources"),
              .testTarget(name: "SegregationNetworkTests",
                          path: "Tests"),
    ],
    swiftLanguageVersions: [.v5]
)
