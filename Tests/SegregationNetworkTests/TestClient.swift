//
//  TestClient.swift
//  SegregationNetworkTests
//
//  Created by Goston on 31/5/20.
//

import Foundation
@testable import SegregationNetwork

class TestClient: NetworkClient {
    let shouldSuccess: Bool
    
    init(shouldSuccess: Bool) {
        self.shouldSuccess = shouldSuccess
    }
    
    func send<T>(_ endpoint: T, completion: @escaping (Result<T.DataType, Error>) -> Void) where T : Endpoint {
        if shouldSuccess {
            let data: Data = try! JSONEncoder().encode(TestModel(id: "0", name: "Test"))
            completion(endpoint.decode(data))
        } else {
            completion(.failure(ResponseError.invalidData))
        }
    }
    
    func cancel() {}
}
