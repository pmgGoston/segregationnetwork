//
//  TestModel.swift
//  SegregationNetworkTests
//
//  Created by Goston on 31/5/20.
//

import Foundation

struct TestModel: Codable {
    let id: String
    let name: String
}
