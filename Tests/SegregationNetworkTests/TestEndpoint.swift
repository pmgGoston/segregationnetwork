//
//  TestEndpoint.swift
//  SegregationNetworkTests
//
//  Created by Goston on 31/5/20.
//

import Foundation
@testable import SegregationNetwork

struct TestEndpoint: Endpoint {
    typealias DataType = TestModel
    
    let scheme: SchemeType = .http
    
    let host: String = "www.google.com"
    
    let path: String = "maps"
    
    let authorization: Authorization = .bearer
    
    let headerFields: [String : String] = [:]
    
    let content: ContentType = .json
    
    let method: HttpMethod = .get
    
    let body: HttpBody = .parameter([:])
}
