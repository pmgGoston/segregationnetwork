import XCTest
@testable import SegregationNetwork

final class SegregationNetworkTests: XCTestCase {
    func testSession() {
        let endpoint: TestEndpoint = TestEndpoint()
        TestClient(shouldSuccess: true).send(endpoint) { (result) in
            switch result {
            case .success(let value):
                XCTAssert(true)
            case .failure(let error):
                XCTAssert(false)
            }
        }
    }

    static var allTests = [
        ("testExample", testSession),
    ]
}
