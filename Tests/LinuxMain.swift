import XCTest

import SegregationNetworkTests

var tests = [XCTestCaseEntry]()
tests += SegregationNetworkTests.allTests()
XCTMain(tests)
