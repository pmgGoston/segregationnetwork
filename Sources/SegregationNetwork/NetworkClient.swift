//
//  NetworkClient.swift
//  SegregationNetwork
//
//  Created by Goston on 27/5/20.
//

import Foundation

public typealias NetworkClient = SegregativeClient & CancellableClient

public protocol SegregativeClient {
    func send<T: Endpoint>(_ endpoint: T, completion: @escaping (Result<T.DataType, Error>) -> Void)
}

public protocol CancellableClient {
    func cancel()
}
