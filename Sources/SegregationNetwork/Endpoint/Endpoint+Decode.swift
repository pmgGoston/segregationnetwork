//
//  Endpoint+Decode.swift
//  SegregationNetwork
//
//  Created by Goston on 27/5/20.
//

import Foundation

public extension Endpoint {
    func decode(_ data: Data) -> Result<DataType, Error> {
        guard !data.isEmpty else { return .failure(ResponseError.invalidData) }
        
        do {
            return .success(try JSONDecoder().decode(DataType.self, from: data))
        } catch {
            return .failure(ResponseError.invalidContent(error))
        }
    }
    
    func verify(_ response: HTTPURLResponse) -> Result<Void, Error> {
        switch response.statusCode {
        case 200...299:
            return .success(())
        case 400...499:
            return .failure(ResponseError.authorizationError)
        case 500...599:
            return .failure(ResponseError.serverError)
        default:
            return .failure(ResponseError.unknownError("status: \(response.statusCode)"))
        }
    }
}
