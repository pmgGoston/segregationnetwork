//
//  Endpoint.swift
//  SegregationNetwork
//
//  Created by Goston on 27/5/20.
//

import Foundation

public protocol Endpoint {
    associatedtype DataType: Decodable
    
    var scheme: SchemeType { get }
    var host: String { get }
    var path: String { get }
    var authorization: Authorization { get }
    var headerFields: [String: String] { get }
    var content: ContentType { get }
    var method: HttpMethod { get }
    var body: HttpBody { get }
    var urlRequest: Result<URLRequest, Error> { get }
    func decode(_ data: Data) -> Result<DataType, Error>
    func verify(_ response: HTTPURLResponse) -> Result<Void, Error>
}
