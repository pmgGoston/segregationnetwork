//
//  Endpoint+Request.swift
//  SegregationNetwork
//
//  Created by Goston on 27/5/20.
//

import Foundation

public extension Endpoint {
    var urlRequest: Swift.Result<URLRequest, Error> {
        switch method {
        case .get:
            return getRequest
        case .post:
            return postRequest
        case .put:
            return putRequest
        }
    }
    
    private var getRequest: Swift.Result<URLRequest, Error> {
        var components: URLComponents = URLComponents()
        components.scheme = scheme.rawValue
        components.host = host
        components.path = path
        if body.parameter.isEmpty == false {
            components.queryItems = body.parameter.map({ URLQueryItem(name: $0.key, value: $0.value as? String) })
        }
        
        guard let url = components.url else { return .failure(RequestError.invalidURL) }
        var request: URLRequest = URLRequest(url: url)
        request.allHTTPHeaderFields = headerFields
        request.httpMethod = method.rawValue
        return .success(request)
    }
    
    private var postRequest: Swift.Result<URLRequest, Error> {
        var components: URLComponents = URLComponents()
        components.scheme = scheme.rawValue
        components.host = host
        components.path = path
        
        guard let url = components.url else { return .failure(RequestError.invalidURL) }
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headerFields
        request.httpBody = body.data
        return .success(request)
    }
    
    private var putRequest: Swift.Result<URLRequest, Error> {
        var components: URLComponents = URLComponents()
        components.scheme = scheme.rawValue
        components.host = host
        components.path = path
        
        guard let url = components.url else { return .failure(RequestError.invalidURL) }
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headerFields
        request.httpBody = body.data
        return .success(request)
    }
}
