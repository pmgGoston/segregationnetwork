//
//  Endpoint+Properties.swift
//  SegregationNetwork
//
//  Created by Goston on 27/5/20.
//

import Foundation

public enum SchemeType: String {
    case http
    case https
}

public enum ContentType {
    case json
    case upload
    case service
    
    static let key: String = "Content-Type"
    
    var value: String {
        switch self {
        case .json, .service:
            return "application/json"
        case .upload:
            return "image/jpeg"
        }
    }
}

public enum Authorization {
    case none
    case bearer
    case custom(key: String, value: String)
    
    var key: String {
        switch self {
        case .none:
            return ""
        case .bearer:
            return "Authorization"
        case .custom(let key, _):
            return key
        }
    }
    
    var value: String {
        switch self {
        case .none:
            return ""
        case .bearer:
            return "Bearer opPMHwYsp3H04rWz6idgVyzF8rZc5D5n"
        case .custom(_, let value):
            return value
        }
    }
}

public enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
}

public enum HttpBody {
    case parameter([String: Any])
    case data(Data)
    
    var parameter: [String: Any] {
        switch self {
        case .parameter(let value):
            return value
        case .data(_):
            return [:]
        }
    }
    
    var data: Data? {
        switch self {
        case .parameter(let value):
            return try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted)
        case .data(let value):
            return value
        }
    }
}
