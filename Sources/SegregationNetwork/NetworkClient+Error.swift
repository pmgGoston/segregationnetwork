//
//  NetworkClient+Error.swift
//  SegregationNetwork
//
//  Created by Goston on 27/5/20.
//

import Foundation

enum RequestError: Error {
    case invalidURL
    case inexistenceKey
    case unimplementedMethod
}

enum ResponseError: Error {
    case invalidData
    case authorizationError
    case serverError
    case invalidContent(Error)
    case unknownError(String)
}
